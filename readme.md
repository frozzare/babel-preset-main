# @hedytech/babel-preset-main

[![npm (scoped)](https://img.shields.io/npm/v/@hedytech/babel-preset-main)](https://www.npmjs.com/package/@hedytech/babel-preset-main)
[![npm](https://img.shields.io/npm/dt/@hedytech/babel-preset-main)](https://www.npmjs.com/package/@hedytech/babel-preset-main)

Main preset for Hedy projects which uses babel.

This package includes the following presets and plugins:

```
@babel/plugin-proposal-class-properties
@babel/plugin-proposal-decorators
@babel/plugin-proposal-export-default-from
@babel/plugin-proposal-export-namespace-from
@babel/plugin-proposal-nullish-coalescing-operator
@babel/plugin-proposal-object-rest-spread
@babel/plugin-proposal-optional-chaining
@babel/plugin-proposal-throw-expressions
@babel/plugin-syntax-dynamic-import
@babel/plugin-transform-dotall-regex
@babel/plugin-transform-runtime
@babel/preset-env
```

Requires `corejs (v 3+)` and `@babel/core`

## Options

There are a few options to make it easier to work with the preset:

* `modules` - Enables or sets modules for preseet-env (defaults to `auto`).
* `targets` - Targets value for preset-env, if used `mode` will be ignored (defaults to undefined to enable mode).
* `mode` - Flags build mode (`web`, `node` or undefined for default).
    * `node` will set the targets value to `node >= 8`
    * `web` will set the targets value to `> 0.25%, not dead`
    * `undefined/default` will set the targets value to `> 0.25%, not dead, node >= 8`

## Webpack example

Install packages:

```
@babel/core
@babel/runtime-corejs3
@hedytech/babel-preset-main
babel-loader
webpack
webpack-cli
```

Create `webpack.config.js`

```js
module.exports = {
  mode: process.env.NODE_ENV !== 'production' ? 'development' : 'production',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        loader: 'babel-loader'
      }
    ]
  },
  entry: path.join(__dirname, 'src', 'index.js'),
  target: 'web', // or something else https://webpack.js.org/configuration/target/
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index.js',
  }
}
```

## Thanks

Inspired by [@jitesoft/babel-preset-main](https://github.com/jitesoft/babel-preset-main)
